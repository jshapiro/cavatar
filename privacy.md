# Data Security and Privacy Statement

## Overview

This is a data security and privacy statement for the Confluence Avatar Server(cas) plugin for
Confluence.

## Data Storage

The plugin does not store or manage any additional data.

## Privacy

We do not collect any data from your use of our plugin. The only information we receive is usage
statistics and licensing information through Atlassian's marketplace API.
